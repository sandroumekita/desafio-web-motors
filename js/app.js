(function (document, window, $) {
    const Webmotors = function () {

        // Traz as informações da API de Modelo
        this.modeloAPI = function () {
            let response = '';

            $.ajax({
                type: "GET",
                url: "http://desafioonline.webmotors.com.br/api/OnlineChallenge/Make",
                async: false,
                success: function (text) {
                    response = text;
                }
            });

            $.each(response, function (i) {
                $("#brandSelect").append(`<option value="${response[i].ID}">${response[i].Name}</option>`)
            })
        }

        // Traz as informações da API de Marcas
        this.marcaAPI = function () {

            $('#brandSelect').change(function () {
                $("#modelSelect").html('Modelo')
                $("#modelSelect").append(`<option value="modelo">Modelo</option>`)

                let id = $('#brandSelect').val()

                $.ajax({
                    type: "GET",
                    url: `http://desafioonline.webmotors.com.br/api/OnlineChallenge/Model?MakeID=${id}`,
                    async: false,
                    success: function (text) {
                        response = text;
                    }
                });

                $.each(response, function (i) {
                    $("#modelSelect").append(`<option value="${response[i].ID}">${response[i].Name}</option>`)
                })

            })
        }

        // Traz as informações da API de Versões
        this.versionAPI = function () {
            $('#modelSelect').change(function () {
                $("#versionSelect").html('Versão')

                let id = $('#modelSelect').val()
                console.log(id)

                $.ajax({
                    type: "GET",
                    url: `http://desafioonline.webmotors.com.br/api/OnlineChallenge/Version?ModelID=${id}`,
                    async: false,
                    success: function (text) {
                        response = text;
                    }
                });

                $.each(response, function (i) {
                    $("#versionSelect").append(`<option value="${response[i].ID}">${response[i].Name}</option>`)
                })
            })
        }

        // Muda o estilo dos botões de comprar Carro/Moto
        this.styleBuyVehicle = function () {
            $('.btn-car').addClass('btn-car2')

            $('.btn-car').on('click', function () {
                $('.btn-car').addClass('btn-car2')
                $('.btn-bike').removeClass('btn-bike2')

            })

            $('.btn-bike').on('click', function () {
                $('.btn-bike').addClass('btn-bike2')
                $('.btn-car').removeClass('btn-car2')
            })
        }

        this.init = function () {
            this.styleBuyVehicle();
            this.modeloAPI();
            this.marcaAPI();
            this.versionAPI();
        }
    }

    const webmotors = new Webmotors()

    $(window).on('load', function () {
        webmotors.init();
    })
})(document, window, jQuery)